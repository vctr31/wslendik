<?php


  include("db.php");

  $db = new MySQL();
  $json = file_get_contents('php://input');
  if(empty($json)){
    header("Location: https://lendik.com/");
  }else{

    $data1=json_decode($json);
    $user_cliente=$data1->user_clie;
    $concepto=$data1->concepto;
    $total=$data1->total;
    $codigo=$data1->codigo;
    $key_app=$data1->app_id;
    $pass=$data1->pass;
    $user=$data1->user;
    $id_suc=$data1->id_suc;
    $id_vendedor=$data1->id_vendedor;
    $id_est=$data1->id_est;
    $promocion=$data1->promocion;
    $data=[];


    if($db->validate_key($key_app)){
        $consulta = $db->login($user,$pass);

        $rows = array();

        if(!$db->num_rows($consulta)<=0){
            $r =  $db->custom_query($consulta);
            if(password_verify($pass,$r['password'])){
              //actualizar

              $cliente = $db->get_cliente_pago_offline($user_cliente)->fetch_assoc();

              //var_dump($user_cliente."sf");

              $suc = $db->getBy_id($id_suc,'sucursales');
              $zona = $db->getBy_id($suc['zona_horaria'],'zonas_horarias');

              date_default_timezone_set($zona['zona']);
              $fecha =date("Y-m-d");
              $hora = date("H:i:s");

              $cantidad = $total;


              $objDate =  new DateTime('NOW');
              $horaCodigo = $objDate->format('H');
              $minutosCodigo1 = $objDate->format('i');
              $minutosCodigo2 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo3 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo4 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo5 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo6 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo7 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo8 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo9 = $objDate->modify('-1 minutes')->format('i');
              $minutosCodigo10 = $objDate->modify('-1 minutes')->format('i');

              $pi = 3.1416;
              $id = intval($cliente['id']);

              $codigoId = ceil($id * $pi);

              $fechas = explode('-',$fecha);
              $explode_year = $fechas[0];
              $explode_month = $fechas[1];
              $explode_day = $fechas[2];

              $nueva_fecha = $explode_year.$explode_month.$explode_day;

              $avrg_cobros = $db->avrg_cobros($cliente['id']);

          		$cliente_temp = $db->getBy_id($cliente['id'],'clientes');

          		if ($cliente['tipo_persona']==2) {

          			$estado_cuenta = $this->estado_cuenta($cliente_temp['id_cliente_empresarial'],true,$db);
          			$suma1=0;
          			$suma2=0;
          			$suma3=1;
          			$suma_cargos_extension = $db->suma_cobros_extension($cliente['id'],$cliente['id_cliente_empresarial']);
          			$estado_cuenta = $this->estado_cuenta($cliente['id_cliente_empresarial'],true,$db);
          			$suma1 = $suma_cargos_extension['suma'];
                $suma2 = floor(abs(floatval($estado_cuenta['suma_pagos']) - floatval($estado_cuenta['saldo_total_corte'])));
          			//$suma2 = $estado_cuenta['suma_pagos'];
          			$suma3 = $db->suma_cobros_extensionAll($cliente['id_cliente_empresarial']);
          			$limite = $estado_cuenta['credito_disponible'];

          			$total_disponible_extension = ($cliente['credit']-$suma1)+($suma2*($suma1/$suma3));
                //$total_disponible_extension=99999;
                var_dump($total_disponible_extension);
          		}
          		else
          		{
          			$estado_cuenta = estado_cuenta($cliente['id'],false,$db);
          		}

              //creamos codigo de verificacion para el cobro.
          		$codigo1 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo1.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo2 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo2.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo3 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo3.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo4 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo4.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo5 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo5.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo6 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo6.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo7 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo7.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo8 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo8.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo9 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo9.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);
          		$codigo10 = $cliente['cellPhone'].$horaCodigo.$minutosCodigo10.number_format($cantidad,2,'.','').$nueva_fecha.$cliente['pin'].$codigoId.floatval($avrg_cobros['avrgCobros']);

          		$code1 = $codigo1;
          		$code2 = $codigo2;
          		$code3 = $codigo3;
          		$code4 = $codigo4;
          		$code5 = $codigo5;
          		$code6 = $codigo6;
          		$code7 = $codigo7;
          		$code8 = $codigo8;
          		$code9 = $codigo9;
          		$code10 = $codigo10;

          		$codigo1 = crc32(md5($codigo1));
          		$codigo2 = crc32(md5($codigo2));
          		$codigo3 = crc32(md5($codigo3));
          		$codigo4 = crc32(md5($codigo4));
          		$codigo5 = crc32(md5($codigo5));
          		$codigo6 = crc32(md5($codigo6));
          		$codigo7 = crc32(md5($codigo7));
          		$codigo8 = crc32(md5($codigo8));
          		$codigo9 = crc32(md5($codigo9));
          		$codigo10 = crc32(md5($codigo10));

              $last_code = $db->lastCode($cliente['id']);
          		$codigo_validado = 0;
          		if($last_code['codigo_offline'] != null)
          		{
          			if($codigo == $last_code['codigo_offline'])
          			{
          				$codigo_validado = 1;
          			}
          		}

              if(floatval($estado_cuenta['credito_disponible'])>=floatval($total))
              {

                $datos_sucursal =  $db->getBy_id($id_suc,'sucursales');
                $tipo_sucursal = intval($datos_sucursal['active']);

                //si es empresarial checar el credito de la extension
                if($cliente['tipo_persona']==2)
                {
                  if ($total_disponible_extension>=floatval($total)) {
                    if($codigo_validado == 0 && ($codigo == $codigo1 || $codigo == $codigo2 || $codigo == $codigo3 || $codigo == $codigo4 || $codigo == $codigo5 || $codigo == $codigo6 || $codigo == $codigo7 || $codigo == $codigo8 || $codigo == $codigo9 || $codigo == $codigo10))
                    {
                      $puntos = floor(floatval($total)*.1);
                        $insert_data = array(
                        'id_est' => $id_est,
                        'id_suc' => $id_suc,
                        'id_vendedor' => $id_vendedor,
                        'id_cliente' => $cliente['id'],
                        'total' => $total,
                        'date' => $fecha,
                        'time' => $hora,
                        'concepto' => $concepto,
                        'type' => 2,
                        'active' => 1,
                        'promocion' => $promocion,
                        'codigo_offline' => $codigo,
                        'puntos' => $puntos
                      );


                      if ($tipo_sucursal == 2) { //PAGO DEMO
                        if($temp = $db->insert($insert_data,'cobros_demo'))
                        {

                          //$db->update_puntos($puntos,$cliente['id']); NO HAY PUNTOS
                          $cobro = $db->getBy_id($temp['id'],'cobros_demo');
                          $hash = crc32($cobro['id'].$cobro['date'].$cobro['time'].$cobro['total']);
                          $db->update(array('hash'=>$hash),$temp['id'],'cobros_demo');

                          email_success_offline($temp['id'],$db,true);
                          echo json_encode(array('error'=>false));
                        }
                      } else { //PAGO REAL
                        if($temp = $db->insert($insert_data,'cobros'))
                        {

                          $db->update_puntos($puntos,$cliente['id']);
                          $cobro = $db->getBy_id($temp['id'],'cobros');
                          $hash = crc32($cobro['id'].$cobro['date'].$cobro['time'].$cobro['total']);
                          $db->update(array('hash'=>$hash),$temp['id'],'cobros');

                          email_success_offline($temp['id'],$db);
                          echo json_encode(array('error'=>false));
                        }
                      }

                    }
                    else
                    {
                      // echo json_encode(array('error'=>true,'msj'=>'El Codigo esta Incorrecto.','titulo'=>'Cobro Cancelado!','credito'=>$estado_cuenta['credito_disponible'],'1'=>$code1,'2'=>$code2,'3'=>$code3,'4'=>$code4,'5'=>$code5));

                      echo json_encode(array('error'=>true,'msj'=>'El Codigo esta Incorrecto.','titulo'=>'Cobro Cancelado!'));
                    }
                  }
                  else
                  {
                    echo json_encode(array('error'=>true,'msj'=>'Credito insuficiente en la extensión.','titulo'=>'Cobro Cancelado!'));
                  }
                }
                else
                { //ES CLIENTE NORMAL
                  if($codigo_validado == 0 && ($codigo == $codigo1 || $codigo == $codigo2 || $codigo == $codigo3 || $codigo == $codigo4 || $codigo == $codigo5 || $codigo == $codigo6 || $codigo == $codigo7 || $codigo == $codigo8 || $codigo == $codigo9 || $codigo == $codigo10))
                    {
                      $puntos = floor(floatval($total)*.1);
                        $insert_data = array(
                        'id_est' => $id_est,
                        'id_suc' => $id_suc,
                        'id_vendedor' => $id_vendedor,
                        'id_cliente' => $cliente['id'],
                        'total' => $total,
                        'date' => $fecha,
                        'time' => $hora,
                        'concepto' => $concepto,
                        'type' => 2,
                        'active' => 1,
                        'promocion' => $promocion,
                        'codigo_offline' => $codigo,
                        'puntos' => $puntos
                      );

                      if ($tipo_sucursal == 2) { //PAGO DEMO
                        if($temp = $db->insert($insert_data,'cobros_demo'))
                        {
                          //NO HAY PUNTOS
                          $cobro = $db->getBy_id($temp['id'],'cobros_demo');
                          $hash = crc32($cobro['id'].$cobro['date'].$cobro['time'].$cobro['total']);
                          $db->update(array('hash'=>$hash),$temp['id'],'cobros_demo');

                          email_success_offline($temp['id'],$db,true);

                          echo json_encode(array('error'=>false));
                        }

                      } else { //PAGO REAL
                        if ( $temp = $db->insert($insert_data,'cobros'))
                        {
                          $db->update_puntos($puntos,$cliente['id']);
                          $cobro = $db->getBy_id($temp['id'],'cobros');
                          $hash = crc32($cobro['id'].$cobro['date'].$cobro['time'].$cobro['total']);
                          $db->update(array('hash'=>$hash),$temp['id'],'cobros');

                          email_success_offline($temp['id'],$db);

                          echo json_encode(array('error'=>false));
                        }
                      }
                    }
                    else
                    {
                      // echo json_encode(array('error'=>true,'msj'=>'El Codigo esta Incorrecto.','titulo'=>'Cobro Cancelado!','credito'=>$estado_cuenta['credito_disponible'],'1'=>$code1,'2'=>$code2,'3'=>$code3,'4'=>$code4,'5'=>$code5));

                      echo json_encode(array('error'=>true,'msj'=>'El Codigo esta Incorrecto.','titulo'=>'Cobro Cancelado!'));
                    }
                }

              }
              else
              {
                //'1'=>$code1,'2'=>$code2,'3'=>$code3,'4'=>$code4,'5'=>$code5
                echo json_encode(array('error'=>true,'msj'=>'Credito insuficiente.','titulo'=>'Cobro Cancelado!'));
              }




            }else{
              $arrayName = array('error' =>'pass no correct',"status"=>'2');
              header('Content-type: application/json; charset=utf-8');
              print json_encode($arrayName);
            }
          }else{

            $arrayName = array('error' =>'user no correct',"status"=>'1');
            header('Content-type: application/json; charset=utf-8');
            print json_encode($arrayName);

          }


    }else{

      $arrayName = array('error' =>'app novalida',"status"=>'3');
      header('Content-type: application/json; charset=utf-8');
      print json_encode($arrayName);

    }

  }

  function estado_cuenta($id,$tipo=false,$db){

		$arreglo = array();


		//si el cliente es una extension de credito
		if ($tipo==true) {

			$data = $db->get_idExterno_empresarial($id,'creditos_empresariales');

		}
		else	{

			$data = $db->get_idExterno($id,'creditos_clientes');


		}

		$datos = $data->fetch_array(MYSQLI_ASSOC);

		$id_externo = $datos['id_externo'];
		$limite_credito = $datos['limite_credito'];
		$dia_corte = intval($datos['dia_corte']);
		$CAT = $datos['CAT'];

		$dia_actual = intval(date('d'));
		$mes_actual = (date('m'));
		$ano_actual = (date('Y'));

		$dia_corte_txt = "".$dia_corte;
		if ($dia_corte < 10){
			$dia_corte_txt = "0".$dia_corte;
		}

		if ($dia_actual > $dia_corte){ // Mi fecha de hoy es mayot al inico de mi corte
			$fecha_ant = $ano_actual."-".$mes_actual."-".$dia_corte_txt;
			$fecha_desp = date('Y-m-d', strtotime("+1 months", strtotime($fecha_ant)));
		} else { // Mi fecha de hoy es menor al inicio de mi corte
			 $fecha_desp = $ano_actual."-".$mes_actual."-".$dia_corte_txt;
			 $fecha_ant = date('Y-m-d', strtotime("-1 months", strtotime($fecha_desp)));
		}

		$fecha_periodo = date('Y-m-d', strtotime("-1 days", strtotime($fecha_desp)));

		$fecha_ant = date('Y-m-d', strtotime("-1 months", strtotime($fecha_ant)));
		$fecha_periodo = date('Y-m-d', strtotime("-1 months", strtotime($fecha_periodo)));

		$link_webservice = "https://187.141.66.23/lendik/wslogicsystems.asmx?WSDL";
		$usr = "ClienteWS";
		$pass = "cl13nt3WS+";
		//$StrMetodo = "NuevaDisposicion";
		$StrMetodo = "ConsultarSaldo";
		$object = new stdClass();
		$object->StrEmpresa = 'LENDIK';
		$object->StrClaseNegocios = 'WSBepensa';
		$object->StrMetodo = $StrMetodo;
		$object->StrUser = $usr;
		$object->StrPass = $pass;
		$object->StrParametros = stripslashes("<?xml version=\"1.0\" encoding=\"utf-8\"?>
		<ConsultaSaldo>
		  <IdPrestamo>$id_externo</IdPrestamo>
		  <NumMovimientos>0</NumMovimientos>
		</ConsultaSaldo>");


		$client = new SoapClient($link_webservice);

		$result = $client->MultiWebMethods($object)->MultiWebMethodsResult; //llamamos al web service

		$result = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $result); // cambiamos codificacion a utf-8

		$respuesta_webService2 = new SimpleXMLElement($result);

  //  var_dump($respuesta_webService2);

		if ($respuesta_webService2->Estatus == "OK") {

			$saldo_corte_1 = floatval($respuesta_webService2->SaldoCorte);
			$pago_minimo = floatval($respuesta_webService2->PagoMinimo);
			$saldo_total_corte = floatval($respuesta_webService2->SaldoTotalCorte);
			$saldo_actual = floatval($saldo_corte = $respuesta_webService2->SaldoActual);
			$saldo_total_actual = floatval($respuesta_webService2->SaldoTotalActual);
			$partes_fecha = explode("T",$respuesta_webService2->FechaLimite);
			$fecha_limite = $partes_fecha[0];
			$credito_disponible = $limite_credito-$saldo_total_actual;

      $arreglo['saldo_total_corte'] = "".($fecha_limite);
			$arreglo['fecha_limite'] = "".($fecha_limite);
			$arreglo['fecha_corte'] = "".($fecha_desp);
			$arreglo['pago_no_intereses'] = "".number_format($saldo_corte_1,2);
			$arreglo['pago_minimo'] = "".number_format($pago_minimo,2);
			$arreglo['limite_credito'] = "".number_format($limite_credito,2);
			$arreglo['credito_disponible'] = "".number_format($credito_disponible,2,'.','');
			$arreglo['fecha_inicio'] = "".($fecha_ant);
			$arreglo['fecha_final'] = "".($fecha_periodo);
			$arreglo['cat'] = "".$CAT." %";

			$suma_pagos=0;
			for($i=0;$i<count($respuesta_webService2->Movimientos->Movimiento);$i++){
				$id_cobro_lendik = "".$respuesta_webService2->Movimientos->Movimiento[$i]->IdExterno;

			 	$tipo = "".$respuesta_webService2->Movimientos->Movimiento[$i]->Tipo;
			    $importe = "".$respuesta_webService2->Movimientos->Movimiento[$i]->Importe;

			    if ($tipo=="false") {
			    	$suma_pagos += floatval($importe);
			    }
			}
			$arreglo['suma_pagos'] = $suma_pagos;
			return $arreglo;
		} else {
			echo "error";
		}

			// $db = $db('Ventas');
			// $data_cred = $db->total_cargos_cliente($id);
		 //    $datos_cred = $data_cred->fetch_array(MYSQLI_ASSOC);

			// $saldo_corte_1 = floatval(0);
			// $pago_minimo = floatval(0);
			// $saldo_total_corte = floatval(0);
			// $saldo_actual = floatval(0);
			// $saldo_total_actual = $datos_cred[0];
			// $partes_fecha = "";
			// $fecha_limite = "";
			// $credito_disponible = $limite_credito - $saldo_total_actual;

			$arreglo['fecha_limite'] = "".($fecha_limite);
			$arreglo['fecha_corte'] = "".($fecha_desp);
			$arreglo['pago_no_intereses'] = "".number_format($saldo_corte_1,2);
			$arreglo['pago_minimo'] = "".number_format($pago_minimo,2);
			$arreglo['limite_credito'] = "".number_format($limite_credito,2);
			$arreglo['credito_disponible'] = "".number_format($credito_disponible,2,'.','');
			$arreglo['fecha_inicio'] = "".($fecha_ant);
			$arreglo['fecha_final'] = "".($fecha_periodo);
			$arreglo['cat'] = "".$CAT." %";

			return $arreglo;
	}

  function email_success_offline($id_cobro,$db,$demo=false)
	{

    if($demo==true)
      {
      $cobro = $db->getBy_id($id_cobro,'cobros_demo');
      }
      else
      {
      $cobro = $db->getBy_id($id_cobro,'cobros');
      }

		$cliente = $db->getBy_id($cobro['id_cliente'],'clientes');

		//checamos que tipo de credito es, empresarial o normal
		if($cliente['id_cliente_empresarial']!="" && $cliente['id_cliente_empresarial']!=null):
			$creditos = $db->getBy(array('id_cliente_empresarial'=>$cobro['id_cliente']),'creditos_empresariales',true)->fetch_assoc();
		else:
			$creditos = $db->getBy(array('id_cliente'=>$cobro['id_cliente']),'creditos_clientes',true)->fetch_assoc();
		endif;

		$est = $db->getBy_id($cobro['id_est'],'establecimientos');
		$suc = $db->getBy_id($cobro['id_suc'],'sucursales');

		require_once 'PHPMailerAutoload.php';


			  $foliocuenta = $creditos['folio'];
			  $nombre_establecimientos = $est['name'];
			  $nombre_sucursal = $suc['name'];
			  $fecha = $cobro['date'];
			  $hora = $cobro['time'];
			  $total = $cobro['total'];
			  $codigo_apro = $cobro['hash'];
			  $email_user = $cliente['email'];
			  $razon_social = $est['razon_social'];

			  $texto = '<!DOCTYPE html>
							  <html lang="en">
							  <head>
							    <meta charset="UTF-8">
							    <title>Pago LENDIK</title>
							  </head>
							  <body style="margin:0px;padding:0px">
							  <div style=" width:100%; height:auto;" align="center">
							  <div style="width:600px; height:auto; padding-top:20px; padding-bottom:20px">
							  <div style="width:600px; text-align:center; padding-top:10px;">
							    <a href="http://www.lendik.com"><img src="http://www.lendik.com/images/mail/header.png" width="600" height="135" alt="Lendik"></a>
							    <div style="height:30px"></div>
							    <span style="font-size: 16px;  color: #000000; font-family: Helvetica, Tahoma, Helvetica, sans-serif; text-align:left; !important">Le notificamos que se ha registrado un cargo a su cuenta LENDIK ******'.$foliocuenta.', los datos de la operación son los siguientes:<br><br>
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
								    	<td align="left" valign="top" ><b>Tipo de conexión:</b></td>
								    	<td align="left" valign="top">Offline</td>
										</tr>
										<tr>
								    	<td align="left" valign="top" ><b>Operación:</b></td>
								    	<td align="left" valign="top"> Compra en '.$nombre_establecimientos.'</td>
										</tr>
										<tr>
											<td align="left" valign="top" ><b>Sucursal:</b></td>
											<td align="left" valign="top">'.$nombre_sucursal.'</td>
										</tr>
										<tr>
											<td align="left" valign="top" ><b>Razón social:</b></td>
											<td align="left" valign="top">'.$razon_social.'</td>
										</tr>
										<tr>
								    	<td align="left" valign="top"><b>Fecha y hora de la operación:&nbsp;&nbsp;</b>
								    	<td align="left" valign="top"> '.$fecha.' '.$hora.'</td>
										</tr>
										<tr>
								    	<td align="left" valign="top"><b>Importe:</b></td>
								    	<td align="left" valign="top">$'.$total.' M.N</td>
										</tr>
										<tr>
								    	<td align="left" valign="top"><b>Código de autorización:</b></td>
								    	<td align="left" valign="top">'.$codigo_apro.'</td>
										</tr>
										<tr>
								    	<td align="left" valign="top"><b>Usuario:</b></td>
								    	<td align="left" valign="top">'.$email_user.'</td>
										</tr>
									</table>
							    </span>
							    <div style="height:0px"></div>
							    '.stripcslashes("").'
							    <div style="height:20px"></div>
							    <span style="font-size: 12px;  color: #606060; font-family: Helvetica, Tahoma, Helvetica, sans-serif;">
							    * Te recordamos que puedes revisar tus estados de cuenta desde tu portal en https://clientes.lendik.com<br>
									* Duda o aclaraciones correspondientes con la operación informada en su notificación comuniquese a: 5541646965 o al correo soporte@lendik.com, horario de atención 9am - 7pm de lunes a viernes.
							  </span>
							  </div>
							  <div style="height:30px"></div>
							  <div style="height:1px; width:600px; background-color:#cfcfcf"></div>
							  <div style="height:30px"></div>
							  <img src="http://www.lendik.com/images/mail/footer.png" width="600" height="90" alt="Lendik">
							  <div style="height:20px"></div>
							  <div style="text-align:justify">

							  </div>
							  <div style="height:15px"></div>
							  <span style="font-size: 12px;  color: #a9a9a9; font-family: Helvetica, Tahoma, Helvetica, sans-serif;">&copy; '.date('Y').' LENDIK S.A.P.I. de C.V. Todos los derechos reservados.</span>
							  </div>
							  </div>
							  </body>
							  </html>';

			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->CharSet = "UTF-8";
			$mail->Host       = "smtp.sendgrid.net"; // SMTP server example
			$mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
			$mail->SMTPAuth   = true;                  // enable SMTP authentication
			$mail->Port       = 587;                    // set the SMTP port for the GMAIL server
			$mail->Username   = "lendik"; // SMTP account username example
			$mail->Password   = "azsxdc12Ironman@";        // SMTP account password example
			//Set who the message is to be sent from
			$mail->setFrom("envios@lendik.com", 'LENDIK');
			//Set an alternative reply-to address
			//Set who the message is to be sent to
			//$mail->addAddress($email, $nombre);
			$mail->addAddress($cliente['email'], $cliente['name']);
			//Set the subject line
			$mail->Subject = "Notificación de compra en comercio";
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body

			//echo stripslashes($texto);

			$mail->msgHTML(stripslashes($texto));
			if (!$mail->send()) {
				return false;
			} else {
				return true;
			}

      $headers  = 'MIME-Version: 1.0' . "\r\n";
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
      //echo $cliente['email'];
      //mail($cliente['email'],$texto,$headers);
	}

    ?>
