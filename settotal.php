<?php
include("db.php");

$db = new MySQL();
$json = file_get_contents('php://input');
if(empty($json)){
  header("Location: https://lendik.com/");
}else{

  $data=json_decode($json);
  $key_app=$data->app_id;
  $pass=$data->pass;
  $user=$data->user;
  $id_cobro=$data->id_cobro;
  $total=$data->total;
  $propina=$data->propina;
  $concepto=$data->concepto;
  $promo=$data->promo;

  //var_dump($data);

  if($db->validate_key($key_app)){
      $consulta = $db->login($user,$pass);

      $rows = array();

      if(!$db->num_rows($consulta)<=0){
            $r =  $db->custom_query($consulta);
            if(password_verify($pass,$r['password'])){
              //actualizar

                $cliente_r=$db->get_info_cobro($id_cobro);
                if(!$db->num_rows($cliente_r)<=0){
                $cliente =  $db->custom_query($cliente_r);

                $cliente_temp = $db->getBy_id($cliente['id_cliente'],'clientes');

                if ($cliente['tipo_persona']==2) {


                  $estado_cuenta = estado_cuenta($cliente_temp['id_cliente_empresarial'],true,$db);
            			$suma1=0;
            			$suma2=0;
            			$suma3=1;
            			$suma_cargos_extension = $db->suma_cobros_extension($cliente['id'],$cliente_temp['id_cliente_empresarial']);
            			$estado_cuenta =estado_cuenta($cliente_temp['id_cliente_empresarial'],true,$db);
            			$suma1 = $suma_cargos_extension;
                  $suma2 = floor(abs(floatval($estado_cuenta['suma_pagos']) - floatval($estado_cuenta['saldo_total_corte'])));
            			//$suma2 = $estado_cuenta['suma_pagos'];
            			$suma3 = $db->suma_cobros_extensionAll($cliente_temp['id_cliente_empresarial']);
            			$limite = $estado_cuenta['credito_disponible'];
                  if($suma3==0){
                    $suma3=1;
                  }
            			$total_disponible_extension = ($cliente_temp['credit']-$suma1)+($suma2*($suma1/$suma3));

                  //$total_disponible_extension=99999;
            		}
            		else
            		{
            			$estado_cuenta = estado_cuenta($cliente['id'],false,$db);
                  $total_disponible_extension=$estado_cuenta['credito_disponible'];
            		}


              //echo ($total_disponible_extension." ".$total);
              if (floatval($total_disponible_extension)>=floatval($total)){
                $respuesta=$db->update_querytotal($id_cobro,$total,$propina,$concepto,$promo);
                if($respuesta === TRUE){
                  $arrayName = array('value' =>'succes',"status"=>'200');
                  header('Content-type: application/json; charset=utf-8');
                  print json_encode($arrayName);
                }else {
                  $arrayName = array('value' =>'error',"status"=>'500');
                  header('Content-type: application/json; charset=utf-8');
                  print json_encode($arrayName);
                }
              }else {
                  $arrayName = array('value' =>'error',"status"=>'400');
                  header('Content-type: application/json; charset=utf-8');
                  print json_encode($arrayName);
              }
            }else{
              $arrayName = array('value' =>'error',"status"=>'500');
              header('Content-type: application/json; charset=utf-8');
              print json_encode($arrayName);
            }

          }else{
            $arrayName = array('error' =>'pass no correct',"status"=>'2');
            header('Content-type: application/json; charset=utf-8');
            print json_encode($arrayName);
          }
        }else{

          $arrayName = array('error' =>'user no correct',"status"=>'1');
          header('Content-type: application/json; charset=utf-8');
          print json_encode($arrayName);

        }


  }else{

    $arrayName = array('error' =>'app novalida',"status"=>'3');
    header('Content-type: application/json; charset=utf-8');
    print json_encode($arrayName);

  }

}

function estado_cuenta($id,$tipo=false,$db){

  $arreglo = array();


  //si el cliente es una extension de credito
  if ($tipo==true) {

    $data = $db->get_idExterno_empresarial($id,'creditos_empresariales');

  }
  else	{

    $data = $db->get_idExterno($id,'creditos_clientes');


  }

  $datos = $data->fetch_array(MYSQLI_ASSOC);

  $id_externo = $datos['id_externo'];
  $limite_credito = $datos['limite_credito'];
  $dia_corte = intval($datos['dia_corte']);
  $CAT = $datos['CAT'];

  $dia_actual = intval(date('d'));
  $mes_actual = (date('m'));
  $ano_actual = (date('Y'));

  $dia_corte_txt = "".$dia_corte;
  if ($dia_corte < 10){
    $dia_corte_txt = "0".$dia_corte;
  }

  if ($dia_actual > $dia_corte){ // Mi fecha de hoy es mayot al inico de mi corte
    $fecha_ant = $ano_actual."-".$mes_actual."-".$dia_corte_txt;
    $fecha_desp = date('Y-m-d', strtotime("+1 months", strtotime($fecha_ant)));
  } else { // Mi fecha de hoy es menor al inicio de mi corte
     $fecha_desp = $ano_actual."-".$mes_actual."-".$dia_corte_txt;
     $fecha_ant = date('Y-m-d', strtotime("-1 months", strtotime($fecha_desp)));
  }

  $fecha_periodo = date('Y-m-d', strtotime("-1 days", strtotime($fecha_desp)));

  $fecha_ant = date('Y-m-d', strtotime("-1 months", strtotime($fecha_ant)));
  $fecha_periodo = date('Y-m-d', strtotime("-1 months", strtotime($fecha_periodo)));

  $link_webservice = "https://187.141.66.23/lendik/wslogicsystems.asmx?WSDL";
  $usr = "ClienteWS";
  $pass = "cl13nt3WS+";
  //$StrMetodo = "NuevaDisposicion";
  $StrMetodo = "ConsultarSaldo";
  $object = new stdClass();
  $object->StrEmpresa = 'LENDIK';
  $object->StrClaseNegocios = 'WSBepensa';
  $object->StrMetodo = $StrMetodo;
  $object->StrUser = $usr;
  $object->StrPass = $pass;
  $object->StrParametros = stripslashes("<?xml version=\"1.0\" encoding=\"utf-8\"?>
  <ConsultaSaldo>
    <IdPrestamo>$id_externo</IdPrestamo>
    <NumMovimientos>0</NumMovimientos>
  </ConsultaSaldo>");


  $client = new SoapClient($link_webservice);

  $result = $client->MultiWebMethods($object)->MultiWebMethodsResult; //llamamos al web service

  $result = preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $result); // cambiamos codificacion a utf-8

  $respuesta_webService2 = new SimpleXMLElement($result);

//  var_dump($respuesta_webService2);

  if ($respuesta_webService2->Estatus == "OK") {

    $saldo_corte_1 = floatval($respuesta_webService2->SaldoCorte);
    $pago_minimo = floatval($respuesta_webService2->PagoMinimo);
    $saldo_total_corte = floatval($respuesta_webService2->SaldoTotalCorte);
    $saldo_actual = floatval($saldo_corte = $respuesta_webService2->SaldoActual);
    $saldo_total_actual = floatval($respuesta_webService2->SaldoTotalActual);
    $partes_fecha = explode("T",$respuesta_webService2->FechaLimite);
    $fecha_limite = $partes_fecha[0];
    $credito_disponible = $limite_credito-$saldo_total_actual;

    $arreglo['saldo_total_corte'] = "".($fecha_limite);
    $arreglo['fecha_limite'] = "".($fecha_limite);
    $arreglo['fecha_corte'] = "".($fecha_desp);
    $arreglo['pago_no_intereses'] = "".number_format($saldo_corte_1,2);
    $arreglo['pago_minimo'] = "".number_format($pago_minimo,2);
    $arreglo['limite_credito'] = "".number_format($limite_credito,2);
    $arreglo['credito_disponible'] = "".number_format($credito_disponible,2,'.','');
    $arreglo['fecha_inicio'] = "".($fecha_ant);
    $arreglo['fecha_final'] = "".($fecha_periodo);
    $arreglo['cat'] = "".$CAT." %";

    $suma_pagos=0;
    for($i=0;$i<count($respuesta_webService2->Movimientos->Movimiento);$i++){
      $id_cobro_lendik = "".$respuesta_webService2->Movimientos->Movimiento[$i]->IdExterno;

      $tipo = "".$respuesta_webService2->Movimientos->Movimiento[$i]->Tipo;
        $importe = "".$respuesta_webService2->Movimientos->Movimiento[$i]->Importe;

        if ($tipo=="false") {
          $suma_pagos += floatval($importe);
        }
    }
    $arreglo['suma_pagos'] = $suma_pagos;
    return $arreglo;
  } else {
    echo "error";
  }

    // $db = $db('Ventas');
    // $data_cred = $db->total_cargos_cliente($id);
   //    $datos_cred = $data_cred->fetch_array(MYSQLI_ASSOC);

    // $saldo_corte_1 = floatval(0);
    // $pago_minimo = floatval(0);
    // $saldo_total_corte = floatval(0);
    // $saldo_actual = floatval(0);
    // $saldo_total_actual = $datos_cred[0];
    // $partes_fecha = "";
    // $fecha_limite = "";
    // $credito_disponible = $limite_credito - $saldo_total_actual;

    $arreglo['fecha_limite'] = "".($fecha_limite);
    $arreglo['fecha_corte'] = "".($fecha_desp);
    $arreglo['pago_no_intereses'] = "".number_format($saldo_corte_1,2);
    $arreglo['pago_minimo'] = "".number_format($pago_minimo,2);
    $arreglo['limite_credito'] = "".number_format($limite_credito,2);
    $arreglo['credito_disponible'] = "".number_format($credito_disponible,2,'.','');
    $arreglo['fecha_inicio'] = "".($fecha_ant);
    $arreglo['fecha_final'] = "".($fecha_periodo);
    $arreglo['cat'] = "".$CAT." %";

    return $arreglo;
}
 ?>
