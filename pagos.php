<?php
include("db.php");

$db = new MySQL();
$json = file_get_contents('php://input');
if(empty($json)){
  header("Location: https://lendik.com/");
}else{

  $data=json_decode($json);
  $id_est=$data->id_est;
  $key_app=$data->app_id;
  $id_suc=$data->id_suc;
  $pass=$data->pass;
  $user=$data->user;

  //var_dump($data);

  if($db->validate_key($key_app)){
      $consulta = $db->login($user,$pass);

      $rows = array();

      if(!$db->num_rows($consulta)<=0){
          $r =  $db->custom_query($consulta);


          if(password_verify($pass,$r['password'])){
            $pendientes=$db->get_solicitud_pago($id_est,$id_suc);
            $rr =  $db->custom_query_all($pendientes);
            if(!$db->num_rows($pendientes)<=0){

              $estatus = array('status' => '200');
              header('Content-type: application/json; charset=utf-8');
              echo json_encode(array_merge($rr,$estatus));
            }else{

              $arrayName = array('error' =>'no solicitudes',"status"=>'4');
              header('Content-type: application/json; charset=utf-8');
              print json_encode($arrayName);

            }
            //var_dump($rr);

          }else{
            $arrayName = array('error' =>'pass no correct',"status"=>'2');
            header('Content-type: application/json; charset=utf-8');
            print json_encode($arrayName);
          }
        }else{

          $arrayName = array('error' =>'user no correct',"status"=>'1');
          header('Content-type: application/json; charset=utf-8');
          print json_encode($arrayName);

        }


  }else{

    $arrayName = array('error' =>'app novalida',"status"=>'3');
    header('Content-type: application/json; charset=utf-8');
    print json_encode($arrayName);

  }

}
 ?>
