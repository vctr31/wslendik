
<?php

  include("db.php");

  $db = new MySQL();
  $json = file_get_contents('php://input');
  if(empty($json)){
    header("Location: https://lendik.com/");
  }else{
      $data=json_decode($json);
      $user=$data->user;
      $pass=$data->pass;
      $id_suc=$data->id_suc;
      $id_est=$data->id_est;
      $fecha=$data->fecha;
      $key_app=$data->app_id;

      if($db->validate_key($key_app)){
          $consulta = $db->login($user,$pass);

          $rows = array();

          if(!$db->num_rows($consulta)<=0){
              $r =  $db->custom_query($consulta);
            $estatus = array('status' => '0');

              if(password_verify($pass,$r['password'])){
                  unset($r['password']);
                  $mispagos=$db->mis_cobros($fecha,$id_suc,$id_est);
                  $mis_cobros=$db->custom_query_all($mispagos);
                  if($db->num_rows($mispagos)>0){
                    header('Content-type: application/json; charset=utf-8');
                    echo json_encode(array_merge($mis_cobros,$estatus));
                  }else{
                    $arrayName = array('error' =>'no rows',"status"=>'400');
                    header('Content-type: application/json; charset=utf-8');
                    print json_encode($arrayName);

                  }
              }else{
                $arrayName = array('error' =>'pass no correct',"status"=>'2');
                header('Content-type: application/json; charset=utf-8');
                print json_encode($arrayName);
              }
            }else{

              $arrayName = array('error' =>'user no correct',"status"=>'1');
              header('Content-type: application/json; charset=utf-8');
              print json_encode($arrayName);

            }


      }else{

        $arrayName = array('error' =>'app novalida',"status"=>'3');
        header('Content-type: application/json; charset=utf-8');
        print json_encode($arrayName);

      }
  }

?>
