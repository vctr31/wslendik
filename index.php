
<?php
  header("Access-Control-Allow-Origin: *");
  include("db.php");

  $db = new MySQL();
  $json = file_get_contents('php://input');
  if(empty($json)){
    header("Location: https://lendik.com/");
  }else{
      $data=json_decode($json);
      $user=$data->user;
      $pass=$data->pass;
      $key_app=$data->app_id;

      if($db->validate_key($key_app)){
          $consulta = $db->login($user,$pass);

          $rows = array();

          if(!$db->num_rows($consulta)<=0){
              $r =  $db->custom_query($consulta);
            $estatus = array('status' => '0');
            // var_dump(password_hash(md5($pass),PASSWORD_DEFAULT));
              $haspass=$db->custom_query($db->get_passforuser($user));
              //var_dump($haspass['password']);
              if(password_verify($pass,$haspass['password'])){
                  unset($r['password']);
                  header('Content-type: application/json; charset=utf-8');
                  echo json_encode(array_merge($r,$estatus));
              }else{
                $arrayName = array('error' =>'pass no correct',"status"=>'2');
                header('Content-type: application/json; charset=utf-8');
                print json_encode($arrayName);
              }
            }else{

              $arrayName = array('error' =>'user no correct',"status"=>'1');
              header('Content-type: application/json; charset=utf-8');
              print json_encode($arrayName);

            }


      }else{

        $arrayName = array('error' =>'app novalida',"status"=>'3');
        header('Content-type: application/json; charset=utf-8');
        print json_encode($arrayName);

      }
  }

?>
