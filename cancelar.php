<?php
header("Access-Control-Allow-Origin: *");
include("db.php");

$db = new MySQL();
$json = file_get_contents('php://input');
if(empty($json)){
  header("Location: https://lendik.com/");
}else{

  if ( isset($_POST['app_id']) ) {
    $key_app=$_POST['app_id'];
    $pass=$_POST['pass'];
    $user=$_POST['user'];
    $id_cobro=$_POST['id_cobro'];

  }else{
    $data=json_decode($json);
    $key_app=$data->app_id;
    $pass=$data->pass;
    $user=$data->user;
    $id_cobro=$data->id_cobro;
  }


  //var_dump($data);

  if($db->validate_key($key_app)){
      $consulta = $db->login($user,$pass);

      $rows = array();

      if(!$db->num_rows($consulta)<=0){
          $r =  $db->custom_query($consulta);
          if(password_verify($pass,$r['password'])){
            //actualizar
              $respuesta=$db->delete_cobro($id_cobro);

              if($respuesta === TRUE){
                $arrayName = array('value' =>'succes',"status"=>'200');
                header('Content-type: application/json; charset=utf-8');
                print json_encode($arrayName);
              }else {
                $arrayName = array('value' =>'error',"status"=>'500');
                header('Content-type: application/json; charset=utf-8');
                print json_encode($arrayName);
              }

          }else{
            $arrayName = array('error' =>'pass no correct',"status"=>'2');
            header('Content-type: application/json; charset=utf-8');
            print json_encode($arrayName);
          }
        }else{

          $arrayName = array('error' =>'user no correct',"status"=>'1');
          header('Content-type: application/json; charset=utf-8');
          print json_encode($arrayName);

        }


  }else{

    $arrayName = array('error' =>'app novalida',"status"=>'3');
    header('Content-type: application/json; charset=utf-8');
    print json_encode($arrayName);

  }

}
 ?>
