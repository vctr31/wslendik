<?php
include("db.php");

$db = new MySQL();
$json = file_get_contents('php://input');
if(empty($json)){
  header("Location: https://lendik.com/");
}else{

  $data=json_decode($json);
  $key_app=$data->app_id;
  $pass=$data->pass;
  $user=$data->user;
  $id_cliente=$data->id_cliente;
  $total=$data->total;
  $fecha=$data->fecha;
  $hora=$data->hora;

  //var_dump($data);

  if($db->validate_key($key_app)){
      $consulta = $db->login($user,$pass);

      $rows = array();

      if(!$db->num_rows($consulta)<=0){
          $r =  $db->custom_query($consulta);
          if(password_verify($pass,$r['password'])){
            //actualizar
               $respuesta=$db->validate_cobro($total,$id_cliente,$fecha,$hora,$key_app);

              switch ($respuesta) {
                case '200':
                  # code...  $arrayName = array('value' =>'succes',"status"=>'200');
                    $arrayName = array('value' =>'succes',"status"=>'200');
                    header('Content-type: application/json; charset=utf-8');
                    print json_encode($arrayName);
                  break;

                  case '300':

                    $arrayName = array('value' =>'not yet',"status"=>'300');
                    header('Content-type: application/json; charset=utf-8');
                    print json_encode($arrayName);

                  break;

                  case '350':

                    $arrayName = array('value' =>'Payment canceled',"status"=>'350');
                    header('Content-type: application/json; charset=utf-8');
                    print json_encode($arrayName);

                  break;

                default:
                  # code...
                  break;
              }

          }else{
            $arrayName = array('error' =>'pass no correct',"status"=>'2');
            header('Content-type: application/json; charset=utf-8');
            print json_encode($arrayName);
          }
        }else{

          $arrayName = array('error' =>'user no correct',"status"=>'1');
          header('Content-type: application/json; charset=utf-8');
          print json_encode($arrayName);

        }


  }else{

    $arrayName = array('error' =>'app novalida',"status"=>'3');
    header('Content-type: application/json; charset=utf-8');
    print json_encode($arrayName);

  }

}
 ?>
